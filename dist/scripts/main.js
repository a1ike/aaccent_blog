"use strict";

$(document).ready(function () {
  /* $(".phone").inputmask({
    mask: "+7(999)-999-99-99",
    showMaskOnHover: false,
  }); */
  $(window).on('load', function () {
    $('body').css('padding-top', $('.b-top').outerHeight());
  });
  $('.b-header-nav li:not(:first-child)').on('click', function (e) {
    e.preventDefault();
    $('.b-header-nav_active').removeClass('b-header-nav_active');
    $(this).addClass('b-header-nav_active');
    $('.b-tags').show();
  });
  $('.b-header-nav li:first-child').on('click', function (e) {
    /* e.preventDefault(); */
    $('.b-header-nav_active').removeClass('b-header-nav_active');
    $(this).addClass('b-header-nav_active');
    $('.b-tags').hide();
  });
  $('.b-header-mob').on('click', function (e) {
    e.preventDefault();
    $('.b-header-mob__open').toggle();
    $('.b-header-mob__close').toggle();
    $('.b-header__logo-text').toggle();
    $('.b-header__search').toggle();
    $('.b-header__info').toggle();
    $('.b-header__social-wrap').toggle();
  });
});
//# sourceMappingURL=main.js.map